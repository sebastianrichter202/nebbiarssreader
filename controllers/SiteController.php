<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\LoginForm;
use app\models\RegisterForm;
use app\models\RssForm;
use yii\base\Exception;

use app\models\User;
use app\models\RssChannel;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Zobrazí prehľad RSS kanálov.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (empty($_SESSION['u'])) {
            return $this->actionLogin();
        }

        return $this->render('index', ['rssChannels' => RssChannel::find()
            ->where(['user_id' => $_SESSION['u']['id']])->all()]);
    }

    /**
     * Zobrazí možnosť pridania RSS kanála.
     *
     * @return string
     */
    public function actionRss($id = 0)
    {
        $model = new RssForm();
        $isEditMode = false;

        if($id != 0) {
            if($result = RssChannel::findOne($id)) {
                $isEditMode = true;
                foreach(['name', 'url'] as $col) {
                    $model->{$col} = $result->{$col};
                }
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            try {
                if(!$isEditMode) {
                    $model->verify();
                    $rssChannel = new RssChannel();
                } else {
                    $rssChannel = $result;
                }

                $rssChannel->name = $model->name;
                $rssChannel->url = $model->url;
                $rssChannel->user_id = $_SESSION['u']['id'];

                if(!$rssChannel->save(false)) {
                    throw new Exception("Vyskytla sa chyba pri ukladaní RSS kanálu. Prosím skúste to znovu.");
                }
                
                Yii::$app->session->setFlash('success', "RSS kanál bol úspešne uložený.");
            } catch (Exception $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
                return $this->goBack();
            }

            return $this->goHome();
        }

        return $this->render('add_rss', [
            'model' => $model
        ]);
    }

    /**
     * Odstránenie kanála
     *
     * @return string
     */
    public function actionDeleterss($id)
    {
        $id = intval($id);

        try {
            
            $rssChannel = RssChannel::findOne($id);
            if(!$rssChannel->delete()) {
                throw new Exception("Nepodarilo sa odstrániť RSS kanál.");
            }

        } catch (Exception $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
            return $this->goBack();
        }

        return $this->goHome();
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!empty($_SESSION['u'])) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goHome();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Register action.
     *
     * @return Response|string
     */
    public function actionRegister()
    {
        if (!empty($_SESSION['u'])) {
            return $this->goHome();
        }

        $model = new RegisterForm();
        if ($model->load(Yii::$app->request->post()) && $model->verify()) {
            
            try {
                $user = new User();
                $user->username = $model->username;
                $user->password = password_hash($model->password, PASSWORD_DEFAULT);
                
                if(!$user->save(false)) {
                    throw new Exception("Uživateľa nebolo možné zaregistrovťa. Prosím skúste to znovu.");
                }
                
                Yii::$app->session->setFlash('success', "Boli ste úspešne zaregistrovaný. Môžte sa prihlásiť.");
            } catch (Exception $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
                return $this->goBack();
            }

            return $this->actionLogin();
        }

        $model->password = '';
        $model->passwordRepeat = '';
        return $this->render('register', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        // Zmažem SESSION pre user login
        unset($_SESSION['u']);

        return $this->goHome();
    }
}
