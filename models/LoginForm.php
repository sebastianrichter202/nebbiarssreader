<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required']
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        $user = $this->getUser();

        if (!empty($user)) {

            if(!password_verify($this->password, $user->password)) {
                Yii::$app->session->setFlash('error', 'Nesprávne meno alebo heslo.');
                return false;
            }

            $_SESSION['u'] = [
                'login' => $user->username,
                'id' => $user->id,
                'group' => User::DEFAULT_GROUP
            ];
            
            return true;
        }

        Yii::$app->session->setFlash('error', 'Nesprávne meno alebo heslo.');
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::find()->where(['username' => $this->username])->one();
        }

        return $this->_user;
    }
}
