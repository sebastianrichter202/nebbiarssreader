<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\base\Exception;

/**
 * RegisterForm is the model behind the login form.
 *
 * @property-read User|null $user This property is read-only.
 *
 */
class RegisterForm extends Model
{
    public $username;
    public $password;
    public $passwordRepeat;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password', 'passwordRepeat'], 'required'],
        ];
    }

    /**
     * Skontroluje či su zhodné heslá
     *
     * @return (bool)
     */
    private function validatePasswords()
    {
        return $this->password == $this->passwordRepeat;
    }

    /**
     * Skontroluje či username (e-mail) je naozaj e-mail
     *
     * @return (bool)
     */
    private function validateUsernameAsEmail()
    {
        return filter_var($this->username, FILTER_VALIDATE_EMAIL);
    }

    /**
     * Skontroluje či už existuje uživ. meno (e-mail)
     *
     * @return (bool)
     */
    private function alreadyExistUserWithUsername()
    {
        return !User::find()
            ->where(['username' => $this->username])
            ->count() > 0;
    }


    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function verify()
    {
        $user = new User();

        try {
            // Skontrolujem či už existuje uživ. meno
            if(!$this->validateUsernameAsEmail()) {
                throw new Exception("Zadaný e-mail je v nesprávnom formáte.", 1);
            }

            // Skontrolujem či už existuje uživ. meno
            if(!$this->alreadyExistUserWithUsername()) {
                throw new Exception("Zadaný e-mail už existuje.", 1);
            }

            // Skontrolujem heslá
            if(!$this->validatePasswords()) {
                throw new Exception("Zadané heslá sa nezhodujú.", 2);
            }

        } catch (Exception $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
            return false;
        }

        return true;
    }
}
