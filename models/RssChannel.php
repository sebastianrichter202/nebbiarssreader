<?php

namespace app\models;
use yii\db\ActiveRecord;

class RssChannel extends ActiveRecord
{
    // public $id;
    // public $name;
    // public $url;
    // public $user_id;

    public static function tableName()
    {
        return 'rss_channels';
    }

    public function rules()
    {
        return [
            [['name', 'url'], 'string', 'max' => 255]
         ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Názov',
            'url' => 'URL'
        ];
    }
}
