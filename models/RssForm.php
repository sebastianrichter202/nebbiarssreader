<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\base\Exception;

class RssForm extends Model
{
    public $id;
    public $name;
    public $url;
    public $userId;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['name', 'url'], 'required'],
        ];
    }

    /*
    * Kontrola či existuje kanál podla URL,
    * ak áno tak príde exception.
    */
    private function alreadyExistWithURL()
    {
        return RssChannel::find()
            ->where(['url' => $this->url])
            ->count() > 0;
    }

    /*
    * Kontrola či existuje kanál podla mena
    */
    private function alreadyExistWithName()
    {
        return RssChannel::find()
            ->where(['name' => $this->name])
            ->count() > 0;
    }


    /**
     * Uloží RSS kanál buď nový alebo v ramci editácie
     * @return bool
     */
    public function verify()
    {
        $user = new RssChannel();
        $this->parseUrl();

        try {

            // Skontrolujem či už existuje kanál s týmto názvom
            // ak áno tak mu prídám 2ku na koniec názvu
            if($this->alreadyExistWithName()) {
                $this->name = $this->name.'2'; 
            }

            // Skontrolujem či už existuje kanál s toutu URL
            if($this->alreadyExistWithURL()) {
                throw new Exception("Kanál s URL '".$this->url."' už máte pridaný.");
            }

        } catch (Exception $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
            return false;
        }

        return true;
    }

    private function parseUrl()
    {
        $url = str_replace(['http://', 'https://', 'www.'], '', $this->url);

        if(substr($url, -1) == '/') {
            $url = substr($url, 0, strlen($url)-1); 
        }

        $url = trim($url);

        $this->url = $url;
    }
}
