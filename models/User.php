<?php

namespace app\models;
use yii\db\ActiveRecord;

class User extends ActiveRecord
{
    // public $id;
    // public $username;
    // public $password;
    // public $created;

    const DEFAULT_GROUP = 'user';

    public static function tableName()
    {
        return 'users';
    }

    public function rules()
    {
        return [
            [['username', 'password'], 'string', 'max' => 255]
         ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'username' => 'E-mail',
            'password' => 'Heslo'
        ];
    }
}
