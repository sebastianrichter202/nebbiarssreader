<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Pridanie RSS kanálu';
if(!empty($model->name)) {
	$this->title = 'Editácia RSS kanálu';
}
?>

<div class="site-index">
	<div class="body-content">
        <div class="row">
            <div class="col-lg-12">
            	
            	<h1><?= Html::encode($this->title) ?></h1>

			    <?php $form = ActiveForm::begin([
			        'id' => 'rss-form',
			        'layout' => 'horizontal',
			        'fieldConfig' => [
			            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
			            'labelOptions' => ['class' => 'col-lg-1 control-label'],
			        ],
			    ]); ?>

			        <?= $form->field($model, 'name')->label('Názov')->textInput(['autofocus' => true]) ?>

			        <?= $form->field($model, 'url')->label('URL') ?>

			        <div class="form-group">
			            <div class="col-lg-offset-1 col-lg-11">
			                <?= Html::submitButton('Uložiť', ['class' => 'btn btn-primary', 'name' => 'rss-button']) ?>
			            </div>
			        </div>

			    <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>