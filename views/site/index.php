<?php

/* @var $this yii\web\View */

$this->title = 'Prehľad RSS kanálov';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>RSS Čítačka</h1>

        <p class="lead">Prehľad RSS kanálov</p>

        <p><a class="btn btn-lg btn-success" href="/site/rss">Pridať nový RSS kanál</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                
                <div class="table-responsive">
                  <table class="table">
                    <thead>
                        <th>#</th>
                        <th>Názov</th>
                        <th>URL</th>
                        <th>Akcia</th>
                    </thead>
                    <tbody class="table-striped">
                        <?php foreach ($rssChannels as $i => $rssChannel): ?>
                            <tr>
                                <td><?= $i+1 ?></td>
                                <td>
                                    <a href="/site/rss/<?= $rssChannel->id ?>">
                                        <?= $rssChannel->name ?>
                                    </a>
                                </td>
                                <td>
                                    <a href="/site/rss/<?= $rssChannel->id ?>">
                                        <?= $rssChannel->url ?>
                                    </a>
                                </td>
                                <td>
                                    <a href="/site/rss/<?= $rssChannel->id ?>" class="btn btn-xs btn-primary">
                                        Upraviť
                                    </a>
                                    <a confirm-title="Naozaj chcete odstrániť tento kanál?" confirm-href="/site/deleterss/<?= $rssChannel->id ?>" class="btn btn-xs btn-danger">
                                        Zmazať
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                  </table>
                </div>

            </div>
        </div>

    </div>
</div>
