<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Prihlásenie';
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Prosím prihláste sa</p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

        <?= $form->field($model, 'username')->label('E-Mail')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'password')->label('Heslo')->passwordInput() ?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Prihlásiť', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
    <hr>
    <h5>Ste tu nový ? <a href="/site/register">Registrácia</a></h5> 
</div>
