$(function(){

	$('[confirm-href]').click(function(){
		var href = $(this).attr('confirm-href');
		var title = $(this).attr('confirm-title');

		if(!title || title == "") {
			title = "Ste si istý ?";
		}

		if(confirm(title)) {
			window.location.href = href;
		}
	});

});